package ru.andrey.dev;
/**
 * Класс {@code Calculator} содержит методы сложения, вычитания, умножения, деления,
 * вычисления остатка от деления, возведения в степень и вычисления квадратного корня.
 *
 * @author Рыжкин А.
 * @version 1.4
 * @since 10.09.2016
 */
/* Программный код*/
public class Calculator {
    /**
     * Возвращает сумму своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  первое слагаемое.
     * @param secondValue второе слагаемое.
     * @return сумма {@code firstValue} + {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double add(double firstValue, double secondValue) {
        double result = firstValue + secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает разность своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  уменьшаемое.
     * @param secondValue вычитаемое.
     * @return разность {@code firstValue} - {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double subtract(double firstValue, double secondValue) {
        double result = firstValue - secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает произведение своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  первый множитель.
     * @param secondValue второй множитель.
     * @return произведение {@code firstValue} * {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double multiplication(double firstValue, double secondValue) {
        double result = firstValue * secondValue;
        if (Double.MAX_VALUE < result || Double.MIN_VALUE > result) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает частное своих аргументов {@code int}, бросая исключение,
     * если присутствует деление на ноль {@code secondValue = 0}.
     *
     * @param firstValue  делимое.
     * @param secondValue делитель.
     * @return частное {@code firstValue} / {@code secondValue}.
     * @throws ArithmeticException если присутствует деление на ноль.
     */
    public static int div(int firstValue, int secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue / secondValue;
    }

    /**
     * Возвращает частное своих аргументов {@code double}, бросая исключение,
     * если присутствует деление на ноль {@code secondValue = 0}.
     *
     * @param firstValue  делимое.
     * @param secondValue делитель.
     * @return частное {@code firstValue} / {@code secondValue}.
     * @throws ArithmeticException если присутствует деление на ноль.
     */
    public static double div(double firstValue, double secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue / secondValue;
    }

    /**
     * Возвращает остаток от деления своих аргументов, бросая исключение,
     * если присутствует деление на ноль {@code secondValue = 0}.
     *
     * @param firstValue  делимое.
     * @param secondValue делитель.
     * @return остаток от деления {@code firstValue} % {@code secondValue}
     * @throws ArithmeticException если присутствует деление на ноль.
     */
    public static int mod(int firstValue, int secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue % secondValue;
    }

    /**
     * Возвращает значение первого аргумента, возведенное в степень второго аргумента,
     * бросая исключение, если результат имеет переполнение {@code double}.
     *
     * @param base   основание.
     * @param degree степень.
     * @return значение {@code base}<sup>{@code degree}</sup>.
     * @throws ArithmeticException если результат возведения операнда в степень имеет вещественное переполнение.
     */
    public static double pow(double base, int degree) {
        double result = 1;
        boolean flag = false;
        if (degree == 0) {
            return 1;
        }
        if (degree < 0) {
            flag = true;
            degree = -degree;
        }
        for (int i = 0; i < degree; i++) {
            result *= base;
        }
        if (Double.MAX_VALUE < result) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        // если степень имеет отрицательное значение
        if (flag) {
            return 1 / result;
        }
        return result;
    }

    /**
     * Возвращает положительный квадратный корень из {@code double} значения, бросая исключения,
     * если значение подкоренного выражения имеет переполнение {@code double} и подкоренное выражение
     * имеет отрицание.
     * @param value значение подкоренного выражения.
     * @return положительный квадратный корень из {@code value}.
     * @throws ArithmeticException если значение подкоренного выражения имеет вещественное переполнение.
     * @throws ArithmeticException если подкоренное выражение имеет отрицание.
     */
    public static double sqrt(double value) {
        if (Double.MAX_VALUE < value) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        if (value == 0) {
            return 0;
        }
        if (value < 0) {
            throw new ArithmeticException("Подкоренное выражение не может быть отрицательным!");
        }
        double rootValue = 1;
        double eps = 1E-15;
        double nx = (rootValue + value / rootValue) / 2;

        while (Math.abs(rootValue - nx) > eps) {
            rootValue = nx;
            nx = (rootValue + value / rootValue) / 2;
        }
        return rootValue;
    }
}