package ru.andrey.dev;
public class Calculator {
    public static double add(double firstValue, double secondValue) {
        double result = firstValue + secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    public static double subtract(double firstValue, double secondValue) {
        double result = firstValue - secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    public static double multiplication(double firstValue, double secondValue) {
        double result = firstValue * secondValue;
        if (Double.MAX_VALUE < result || Double.MIN_VALUE > result) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    public static int div(int firstValue, int secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue / secondValue;
    }

    public static double div(double firstValue, double secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue / secondValue;
    }

    public static int mod(int firstValue, int secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue % secondValue;
    }

    public static double pow(double base, int degree) {
        double result = 1;
        boolean flag = false;
        if (degree == 0) {
            return 1;
        }
        if (degree < 0) {
            flag = true;
            degree = -degree;
        }
        for (int i = 0; i < degree; i++) {
            result *= base;
        }
        if (Double.MAX_VALUE < result) {
            throw new ArithmeticException("Вещественное переполнение!"); // если будет вещественное переполнение
        }
        if (flag) {
            return 1 / result;
        }//fnnmm
        return result;
    }

    public static double sqrt(double value) {
        if (Double.MAX_VALUE < value) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        if (value == 0) {
            return 0;
        }
        if (value < 0) {
            throw new ArithmeticException("Подкоренное выражение не может быть отрицательным!");
        }
        double rootValue = 1;
        double eps = 1E-15;
        double nx = (rootValue + value / rootValue) / 2;

        while (Math.abs(rootValue - nx) > eps) {
            rootValue = nx;
            nx = (rootValue + value / rootValue) / 2;
        }
        return rootValue;
    }
}
