package ru.andrey.dev;

import java.io.*;

/**
 * <h1>
 * Обфускация файла.
 * </h1>
 * <p>
 * Класс {@code CompressionFile} считывает с файла и сжимает исходный код за счет удаления лишних пробелов, знаков табуляции
 * и символов перехода на новую строку. Не удаляет пробелы в строковых константах. Результат записывает в другой файл.
 * </p>
 *
 * @author Андрей Рыжкин
 * @version 1.0
 * @since 11.12.2016
 */
public class CompressionFile {
    /**
     * Метод считывает каждую строку исходного кода с входного указанного файла, выполняется обфускация кода
     * и результат записывает в одну строку в выходной указанный файл.
     *
     * @param pathInputFile  путь входного файла
     * @param pathOutputFile путь выходного файла
     * @throws IOException          если неверный путь файлов или диск защищён от записи файлов.
     * @throws NullPointerException если пустой исходный файл.
     */
    public static void dataFile(String pathInputFile, String pathOutputFile) throws IOException, NullPointerException {
        Check check = new Check();
        String source;
        check.checkFiles(pathInputFile, pathOutputFile);

        BufferedReader inputData = new BufferedReader(new FileReader(pathInputFile));
        BufferedWriter outData = new BufferedWriter(new FileWriter(pathOutputFile));

        final String regex = " +\\/\\/.*|\\/\\/.*|^.*\\/\\/.*";

        while ((source = inputData.readLine()) != null) {
            // удаляем пробел в начале и в конце строки
            source = source.trim();
            // заменить пробелы в строке, любое количество, одним
            source = source.replaceAll(" +", " ");
            if (check.checkLine(source, regex)) {
                outData.write(commentsReplacement(source));
            } else {
                // записываем строку в файл
                outData.write(gapsInTheStatements(source));
            }
        }
        inputData.close();
        outData.close();
    }

    /**
     * Возвращает строку, изменяя вид комментария.
     *
     * @param source строка с комментарием.
     * @return возвращает строку изменнённого комментария.
     */
    public static String commentsReplacement(String source) {
        source = source.replace("//", "/*");
        source += "*/";
        return source;
    }

    /**
     * Возвращает строку, удаляя лишние пробелы.
     * @param source строка с программным кодом
     * @return возвращает строку.
     */
    public static String gapsInTheStatements(String source) {
        Check check = new Check();
        String regex = ".*\".*\"";
        if (!check.checkLine(source, regex)) {
            source = source.replace(" < ", "<");
            source = source.replace(" > ", ">");
            source = source.replace(" = ", "=");
            source = source.replace(" == ", "==");
            source = source.replace(" != ", "!=");
            source = source.replace(" >= ", ">=");
            source = source.replace(" <= ", "<=");
            source = source.replace(" + ", "+");
            source = source.replace(" - ", "-");
            source = source.replace(" * ", "*");
            source = source.replace(" / ", "/");
            source = source.replace(" || ", "||");
            source = source.replace(" && ", "&&");
            return source;
        } else {
            return source;
        }
    }
}
