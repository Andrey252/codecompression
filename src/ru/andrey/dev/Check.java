package ru.andrey.dev;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>
 * Проверка файлов и соответствие строки с регулярным выражением.
 * </h1>
 * <p>
 * Класс {@code Check} выполняет проверку входных и выходных файлов на верно указанный путь, чтение/запись. Проверяет
 * соответствие строки с регулярным выражением.
 * </p>
 *
 * @author Андрей Рыжкин
 * @version 1.0
 * @since 12.12.2016
 */

public class Check {
    public Check() {
    }

    /**
     * Проверяет входной и выходной файл на верно указанный путь к файлам
     * и чтение/запись файлов, бросая исключения, если неверный путь к файлам, пустой файл, защита от записи файлов.
     *
     * @param pathInputFile  путь входного файла.
     * @param pathOutputFile путь выходного файла.
     * @throws IOException          если неверный путь файлов или диск защищён от записи файлов.
     * @throws NullPointerException если пустой исходный файл.
     */
    public void checkFiles(String pathInputFile, String pathOutputFile) throws IOException, NullPointerException {
        BufferedReader inputData = null;
        BufferedWriter outData = null;
        // проверяем верно ли указан путь для открытия исходного файла, права на чтения.
        try {
            inputData = new BufferedReader(new FileReader(pathInputFile));
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        // проверяем запись выходного файла, создаётся ли он!?
        // верно ли указан путь!?
        try {
            outData = new BufferedWriter(new FileWriter(pathOutputFile));
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        // бросаем исключение, если исходный файл пустой
        if (inputData.readLine() == null) {
            throw new NullPointerException("Пустой исходный файл!");
        }
    }

    /**
     * Метод возвращает {@code boolean} значение, проверяя соответствие строки с регулярным выражением.
     * @param source строка для проверки.
     * @param regex регулярное выражение.
     * @return возвращает {@code true} если строка соответствует с регулярным выражением, иначе {@code false}.
     */
    public boolean checkLine(String source, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);
        return m.matches();
    }
}
