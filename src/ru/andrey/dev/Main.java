package ru.andrey.dev;

import java.io.IOException;
import java.util.Scanner;

/**
 * Класс {@code Main} запускает работу программы.
 *
 * @author Андрей Рыжкин
 * @version 1.0
 * @since 11.12.2016
 */
public class Main {
    public static void main(String[] args) throws IOException, NullPointerException {
        int select = inputSelect();
        Scanner reader = new Scanner(System.in);
        String pathInputFile, pathOutFile;

        System.out.println("Путь входного файла");
        pathInputFile = reader.next();
        System.out.println("Путь выходного файла");
        pathOutFile = reader.next();

        taskSelect(select, pathInputFile, pathOutFile);
        System.out.println("Успешно!");
    }

    public static int inputSelect() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Обфускация программного кода Java");
        System.out.println("1 - Удаление документации JavaDoc'ов;");
        System.out.println("2 - Удаление комментариев;");
        System.out.println("3 - Удаление пробелов, знаков табуляции, переходы на новую строку;");
        System.out.println("4 - Интеграция всех требований;");
        System.out.println("Выберите требование: ");
        int select;
        while (!checkSelect(select = reader.nextInt())) {
            System.out.println("Некорректное значение!");
            System.out.println("Выберите требование: ");
        }
        return select;
    }

    public static void taskSelect(int select, String pathInputFile, String pathOutputFile) throws IOException {
        switch (select) {
            case 1:
                RemovingComments.dataFile(1, pathInputFile, pathOutputFile);
                break;
            case 2:
                RemovingComments.dataFile(2, pathInputFile, pathOutputFile);
                break;
            case 3:
                CompressionFile.dataFile(pathInputFile, pathOutputFile);
                break;
            case 4:
                RemovingComments.dataFile(3, pathInputFile, "outtemp");
                CompressionFile.dataFile("outtemp", pathOutputFile);
        }
    }

    public static boolean checkSelect(int select) {
        if (select < 1 || select > 4) return false;
        else return true;
    }
}
