package ru.andrey.dev;

import java.io.*;

/**
 * <h1>
 * Обфускация файла.
 * </h1>
 * <p>
 * Класс {@code RemovingComments} считывает с файла и по выбору пользователя удаляет JavaDoc или комментарии. Результат записывает в другой файл.
 * </p>
 *
 * @author Андрей Рыжкин
 * @version 1.0
 * @since 27.12.2016
 */
public class RemovingComments {
    /**
     * Метод считывает каждого строку исходного кода с входного указанного файла,
     * удаляя все комментарии, и сохраняет в выходной указанный файл.
     *
     * @param select         номер пункта (1 - javaDoc, 2- комментарии, 3 - перечисленнное из п. 1 и 2).
     * @param pathInputFile  путь входного файла
     * @param pathOutputFile путь выходного файла
     * @throws IOException          если неверный путь файлов или диск защищён от записи файлов.
     * @throws NullPointerException если пустой исходный файл.
     */
    public static void dataFile(int select, String pathInputFile, String pathOutputFile) throws IOException {
        String source;
        Check check = new Check();
        // регулярное выражение, по которому находим строки с комментариями.
        final String regex = selectRegex(select);

        // проверяем входной и выходной файл.
        check.checkFiles(pathInputFile, pathOutputFile);

        // создаём два объекта для работы с входным и выходным файлом.
        BufferedReader inputData = new BufferedReader(new FileReader(pathInputFile));
        BufferedWriter outData = new BufferedWriter(new FileWriter(pathOutputFile));

        // считываем с файла каждую строку и с помощью регулярного выражения находим комментарии и удаляем.
        while ((source = inputData.readLine()) != null) {
            if (!check.checkLine(source, regex)) {

                outData.write(source + "\n");
            }
        }
        inputData.close();
        outData.close();
    }

    /**
     * Метод возращает регулярное выражение, с помощью которого выполняется удаление документации JavaDoc, комментариев из файла.
     *
     * @param select номер пункта (1 - javaDoc, 2- комментарии, 3 - перечисленнное из п. 1 и 2).
     * @return возвращает регулярное выражение.
     */
    public static String selectRegex(int select) {
        String regex = null;
        switch (select) {
            case 1:
                regex = " +\\*.*| +\\/\\*\\*.*|\\/\\*\\*";
                break;
            case 2:
                regex = "\\/\\/.*|\\/\\*.*";
                break;
            case 3:
                regex = " +\\*.*| +\\/\\*\\*.*|\\/\\*\\*|\\/\\/.*|\\/\\*.*";
                break;
        }
        return regex;
    }
}
